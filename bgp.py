import telnetlib
import logging
import router
#logging.basicConfig(level=logging.INFO)
import yaml
from robot.api import logger
import time



with open ('topoVars.yaml') as f:
    x=yaml.safe_load(f)
rtr1Data = x['rtr1']
rtr2Data = x['rtr2']
rtr1BgpData = rtr1Data['bgp']
rtr2BgpData = rtr2Data['bgp']




def Interfaceconfig():
    '''This proc get router details from yaml anf configures interfaces'''
    rtr1Login = router.routerLoginHandle(rtr1Data['ipaddr'])
    rtr2Login = router.routerLoginHandle(rtr2Data['ipaddr'])
    router.configureInterface(rtr1Login, rtr1Data['intf']['name'], rtr1Data['intf']['ipaddress'])
    router.configureInterface(rtr2Login, rtr2Data['intf']['name'], rtr2Data['intf']['ipaddress'])
    r1int = rtr1Data['intf']['name']
    r2int = rtr2Data['intf']['name']
    x=router.showCli(rtr1Login, f'show configuration running-config interface {r1int}')
    for i in x:
        logger.console(i)
    logger.console('###########################################################')
    x=router.showCli(rtr2Login, f'show configuration running-config interface {r2int}')
    for i in x:
        logger.console(i)
    logger.console('###########################################################')
    router.routerLogout(rtr1Login)
    router.routerLogout(rtr2Login)

def configurebgprightPwd(pwd1,pwd2):
    '''This proc gets bgp config details from yaml and configures bgp on router'''
    rtr1Login = router.routerLoginHandle(rtr1Data['ipaddr'])
    router.configBgp(rtr1Login,localRid= rtr1BgpData['rid'], neighbor=rtr1BgpData['neighbor'], \
                     localAs=rtr1BgpData['localAs'], remoteAs=rtr1BgpData['neighborAs'],pwd=pwd1)

    rtr2Login = router.routerLoginHandle(rtr2Data['ipaddr'])
    router.configBgp(rtr2Login,localRid= rtr2BgpData['rid'], neighbor=rtr2BgpData['neighbor'], \
                     localAs=rtr2BgpData['localAs'], remoteAs=rtr2BgpData['neighborAs'],pwd=pwd2)

    rtr1Op = router.showCli(rtr1Login,"sh configuration running-config router bgp")
    for i in rtr1Op:
        logger.console(i)
        logger.debug(i)
    logger.console('###########################################################')

    rtr2Op = router.showCli(rtr2Login,"sh configuration running-config router bgp")
    for i in rtr2Op:
        logger.console(i)
        logger.debug(i)
    logger.console('###########################################################')
    router.routerLogout(rtr1Login)
    router.routerLogout(rtr2Login)

def verifybgp(pwdType):
    '''This porc retrieves bgp info from router'''
    logger.console('Show BGP on Router1')
    logger.debug('Show BGP on Router1')
    rtr1Login = router.routerLoginHandle(rtr1Data['ipaddr'])
    neiborIp = rtr1BgpData['neighbor']
    time.sleep(240)
    rtr1Op = router.showCli(rtr1Login,f'sh bgp neighbor {neiborIp} | include "BGP state"')
    check = []
    for i in rtr1Op:
        logger.console(i)
        logger.debug(i)
        if 'Established' in i:
            check = []
            check.append('pass')
            break
        else:
            check.append('fail')
    logger.console('###########################################################')
    logger.console('Show BGP on Router2')
    logger.debug('Show BGP on Router2')
    rtr2Login = router.routerLoginHandle(rtr2Data['ipaddr'])
    neiborIp = rtr2BgpData['neighbor']
    rtr2Op = router.showCli(rtr2Login,f'sh bgp neighbor {neiborIp} | include "BGP state"')
    check= []
    for i in rtr2Op:
        logger.console(i)
        logger.debug(i)
        if 'Established' in i:
            check = []
            check.append('pass')
            break
        else:
            check.append('fail')
    logger.console('###########################################################')

    router.routerLogout(rtr1Login)
    router.routerLogout(rtr2Login)

    if set(check) == {'pass'}:
        return('pass')
    else:
        return('fail')