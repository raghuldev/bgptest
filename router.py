import telnetlib
import logging
logging.basicConfig(level=logging.INFO)

from robot.api import logger
import time


def configureInterface(telnet,interface,ipaddress):
    '''this function accepts device handle, interface and ipaddress adn configures'''
    telnet.write(b'configure terminal\n')
    telnet.read_until(b'#')

    interfaceCon = f'interface {interface}\n'
    intfByte = bytes(interfaceCon, 'utf-8')
    telnet.write(intfByte)
    telnet.read_until(b'#')

    interfaceCon = f'ipv4 address {ipaddress} 255.255.255.252\n'
    intfByte = bytes(interfaceCon, 'utf-8')
    telnet.write(intfByte)
    telnet.read_until(b'#')
    telnet.write(b'commit\n')
    telnet.read_until(b'#')
    telnet.write(b'end\n')





def routerLoginHandle(host):
    ''' this will accept host ip and return host login handle'''
    telnet = telnetlib.Telnet(host)
    telnet.read_until(b"Username:")
    telnet.write(b'cisco\n')
    telnet.read_until(b'Password:')
    telnet.write(b'cisco\n')
    telnet.read_until(b'#')
    telnet.write(b"vt100\n")
    telnet.write(b'terminal length 0\n')
    return(telnet)


def showCli(telnet,cmd):
    '''this function accepts telnet handle and cli-command
    executes command and return output'''
    time.sleep(5)
    show = f'{cmd}\n'
    showByte = bytes(show, 'utf-8')
    telnet.write(showByte)
    telnet.read_until(b'#')
    telnet.write(b'exit\n')
    x=telnet.read_all()

    x=str(x.decode('UTF-8'))
    x=x.split('\n')
    return(x)





def configBgp(telnet,localRid,neighbor,localAs,remoteAs,pwd):
    '''this function accepts telnetHandle, bgp config parametes and configures the bgp configs on router'''


    ###############################
    telnet.write(b'configure terminal\n')
    telnet.read_until(b'#')
    localAsCon = f'router bgp {localAs}\n'
    localAsByte = bytes(localAsCon, 'utf-8')
    telnet.write(localAsByte)
    telnet.read_until(b'#')
    ridCon = f'bgp router-id {localRid}\n'
    ridConByte = bytes(ridCon, 'utf-8')
    telnet.write(ridConByte)
    telnet.read_until(b'#')
    telnet.write(b'address-family ipv4 unicast\n')
    telnet.read_until(b'#')
    telnet.write(b'exit\n')
    telnet.read_until(b'#')
    neiCon = f'neighbor {neighbor}\n'
    neiByte = bytes(neiCon, 'utf-8')
    telnet.write(neiByte)
    telnet.read_until(b'#')

    pwdCon = f'password {pwd}\n'
    pwdByte = bytes(pwdCon, 'utf-8')
    telnet.write(pwdByte)
    telnet.read_until(b'#')

    remAsCon = f'remote-as {remoteAs}\n'
    remAsByte = bytes(remAsCon, 'utf-8')
    telnet.write(remAsByte)
    telnet.read_until(b'#')
    telnet.write(b'address-family ipv4 unicast\n')
    telnet.read_until(b'#')
    telnet.write(b'commit\n')
    telnet.read_until(b'#')
    telnet.write(b'end\n')
    telnet.read_until(b'#')





def routerLogout(telnet):
    telnet.write(b'exit\n')


