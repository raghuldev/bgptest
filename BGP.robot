*** Settings ***
Documentation    Suite description
Library   bgp.py


*** Test Cases ***
Test
    Interfaceconfig
    configurebgprightPwd    'abc1234'       'abc1234'
    ${result}=      verifybgp
    BuiltIn.Should Be Equal As Strings   ${result}   pass
    configurebgprightPwd    'abc124'       'abc1234'
    ${result}=      verifybgp
    BuiltIn.Should Be Equal As Strings   ${result}   fail